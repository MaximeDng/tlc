# TLC Project

  Group members : Gauthier ROULEAU / Arnaud GOHIER / Maxime DONGE

## Google AppEngine related files

  * `/src/main/webapp/WEB-INF/appengine-web.xml` 
  * `/src/main/webapp/WEB-INF/datastore-indexes.xml` 

## Running locally

```
mvn appengine:devserver
```

And go to http://127.0.0.1:8080

## Deploying to Google Cloud

```
mvn appengine:update
```

## API endpoints

- search: `GET /api/`
- bulk add: `POST /api/run`
  - body:
  ```json
  [
     {"id":9,"lat":48.8601,"lon":2.3507,"user":"leo","timestamp":1543775727},
     {"id":27,"lat":48.8601,"lon":2.3507,"user":"toto","timestamp":1543775727},
     {"id":9,"lat":48.8601,"lon":2.3507,"user":"leo","timestamp":1543775727}
  ]
  ```
- bulk delete `DELETE /api/run/1,2,3`

## API benchmark
Nous avons décidé de comparer les temps de réponses lors de deux requête consécutives `POST` contenant 500 entités.

### Exécution du benchmark avec le *test runner* de Postman

![API Benchmark](./assets/benchmark.png)

### Résultats

- temps de réponse de la première requête: `15057ms`
- temps de réponse de la deuxième requête: `7652ms`

### Conclusion
La solution de Google permet une scalabilité des ressources nécessaires au traitement des reqêtes qui lui sont adressées.