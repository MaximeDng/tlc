package tlc.tracking;


import org.restlet.data.Form;
import org.restlet.data.Parameter;
import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

public class RunResource extends ServerResource {

    private final RecordService recordService = new RecordService();


    @Post("json")
    public void bulkAdd(RecordList toAdd) {
        recordService.bulkAdd(toAdd);

    }

    @Get("json")
    public RecordList search() {
        Form form = getRequest().getResourceRef().getQueryAsForm();
        return recordService.search(form);
    }

    @Delete("json")
    public void bulkDelete() {
        String[] run_ids = getRequest().getAttributes().get("list").toString().split(",");
        recordService.delete(run_ids);

        //@FIXME You must delete every records that contain one of the run_id in run_ids
    }

}