package tlc.tracking;

import com.google.cloud.datastore.*;
import org.restlet.data.Form;
import org.restlet.data.Parameter;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RecordService {

    // Create an authorized Datastore service using Application Default Credentials.
    private final Datastore datastore = DatastoreOptions.getDefaultInstance().getService();

    private final KeyFactory keyFactory = datastore.newKeyFactory().setKind("Record");

    public void bulkAdd(RecordList toAdd){
        List<Entity> entities = new ArrayList<>();
        Key key;
        for(Record r : toAdd){
            key = datastore.allocateId(keyFactory.newKey());

            Entity record = Entity.newBuilder(key)
                    .set("id", r.id)
                    .set("lat", r.lat)
                    .set("lon",r.lon)
                    .set("user",r.user)
                    .set("timestamp",r.timestamp)
                    .build();
            entities.add(record);
        }

        Entity[] arrEntites = batch(Entity.class,entities);
        datastore.put(arrEntites);
    }

    public RecordList search(Form form){
        RecordList res = new RecordList();
        boolean isLngLat = false;
        Map<String, String> paramsMap = getParams(form);
        Query<Entity> query;
        for (String str : form.getNames()){
            if (str.equals("area")){
                isLngLat = true;
            }
        }

        if (isLngLat){
            query = queryLngLat(paramsMap);
        }
        else {
            query = queryTimestamp(paramsMap);
        }

        QueryResults<Entity> tasks = datastore.run(query);
        Record record;

        while (tasks.hasNext()){
            Entity e = tasks.next();
            record = new Record(e.getLong("id"),e.getDouble("lat"),e.getDouble("lon")
                    ,e.getString("user"),e.getLong("timestamp"));
            res.add(record);
        }

        return res;
    }

    public List<Key> findKeysById(String entityName, String propertyValue) {
        List<Key> keys = new ArrayList<>();
        String gql = "SELECT * FROM "+entityName +" WHERE id="+propertyValue;
        Query<Entity> query = Query.newGqlQueryBuilder(Query.ResultType.ENTITY, gql)
                .setAllowLiteral(true).build();

        QueryResults<Entity> results = datastore.run(query);
        while (results.hasNext()) {
            Entity rs = results.next();
            keys.add(rs.getKey());

        }
        return keys;
    }

    public void delete(String[] records_ids){
        List<Key> keys = new ArrayList<>();
        for (int i = 0; i < records_ids.length;i++){
            String currentRecord = records_ids[i];
            keys.addAll(findKeysById("Record", currentRecord));
        }
        datastore.delete(batch(Key.class,keys));
    }

    private Map<String, String> getParams(Form form){
        Map<String, String> paramsMap = new HashMap<String, String>();
        for (Parameter parameter : form){
            paramsMap.put(parameter.getName(),parameter.getValue());
        }
        return paramsMap;
    }

    private Query<Entity> queryTimestamp(Map<String, String> paramsMap){
        String[] arrTimeStamp = paramsMap.get("timestamp").split(",");
        int timestampMin = Integer.valueOf(arrTimeStamp[0]);
        int timestampMax = Integer.valueOf(arrTimeStamp[1]);

        return Query.newEntityQueryBuilder()
                .setKind("Record")
                .setFilter(StructuredQuery.CompositeFilter.and(StructuredQuery.PropertyFilter.eq("user",paramsMap.get("user"))
                        , StructuredQuery.PropertyFilter.ge("timestamp",timestampMin),
                        StructuredQuery.PropertyFilter.le("timestamp", timestampMax)))
                .build();


    }

    private Query<Entity> queryLngLat(Map<String, String> paramsMap){
        String[] arrTimeStamp = paramsMap.get("timestamp").split(",");
        String[] arrArea = paramsMap.get("area").split(",");
        String user = paramsMap.get("user");

        int timestampMin = Integer.valueOf(arrTimeStamp[0]);
        int timestampMax = Integer.valueOf(arrTimeStamp[1]);
        double lat = Double.valueOf(arrArea[0]);
        double lng = Double.valueOf(arrArea[1]);

        return Query.newEntityQueryBuilder()
                .setKind("Record")
                .setFilter(StructuredQuery.CompositeFilter.and(StructuredQuery.PropertyFilter.eq("user",user)
                        , StructuredQuery.PropertyFilter.ge("timestamp",timestampMin),
                        StructuredQuery.PropertyFilter.le("timestamp", timestampMax),
                        StructuredQuery.PropertyFilter.eq("lat",lat),
                        StructuredQuery.PropertyFilter.eq("lon",lng)))
                .build();
    }


    private <T> T[] batch(Class<T> c,  List<T> g) {
        @SuppressWarnings("unchecked")
        T[] res = (T[]) Array.newInstance(c, g.size());
        g.toArray(res);
        return res;
    }


}
